add_executable( BulletExperiments main.cpp )

include_directories(${BULLET_INCLUDE_DIR})

target_link_libraries(BulletExperiments ${BULLET_LIBRARIES})
